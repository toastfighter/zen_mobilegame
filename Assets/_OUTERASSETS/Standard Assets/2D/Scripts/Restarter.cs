using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D
{
    public class Restarter : MonoBehaviour
    {
        [SerializeField]
        public Animator foxAnimator;

        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                foxAnimator.SetTrigger("Die!");
                StartCoroutine(WaitForDieAnim());
            }
        }
        
        IEnumerator WaitForDieAnim()
        {
            yield return new WaitForSeconds(4);
            SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
        }
    }
}
