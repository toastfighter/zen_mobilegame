﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMat : MonoBehaviour
{
    // Start is called before the first frame update

    //Renderer spriteRenderer;

    void Start()
    {
        Renderer renderer = GetComponent<Renderer>();
        if (renderer == null)
            Debug.Log("Renderer is empty");
        GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        GetComponent<Renderer>().receiveShadows = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
