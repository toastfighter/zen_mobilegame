﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudDisappear : MonoBehaviour
{
    public Animator Cloud1;
    public Animator Cloud2;
    public Animator Cloud3;
    // Start is called before the first frame update
    void Start()
    {
        Cloud1.Play("Cloud_2_anim");
        Cloud2.Play("Cloud_3_anim");
        Cloud3.Play("Cloud_5_anim");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Cloud1.Play("Cloud_2_anim");
        Cloud2.Play("Cloud_3_anim");
        Cloud3.Play("Cloud_5_anim");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
