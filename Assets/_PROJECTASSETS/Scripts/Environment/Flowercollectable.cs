﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flowercollectable : MonoBehaviour
{

    public ParticleSystem collectEffect;
    public AudioSource collectableSound;
    // Start is called before the first frame update
    void Start()
    {
        collectEffect.Stop();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        
        collectEffect.Play();
        collectableSound.Play();
        Destroy(gameObject);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
