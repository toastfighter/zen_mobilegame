﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MenuUIManager : MonoBehaviour
{

    
    public RectTransform mainMenu, SelectionMenu, OptionsMenu, CreditsMenu;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChooseLevel()
    {
        mainMenu.DOAnchorPos(new Vector2(-800, 0), 0.9f);
        SelectionMenu.DOAnchorPos(new Vector2(0, -40), 0.9f);
        Debug.Log("Clicked");
    }
    public void CloseChooseLevel()
    {
        mainMenu.DOAnchorPos(new Vector2(0, 0), 0.9f);
        SelectionMenu.DOAnchorPos(new Vector2(800, 0), 0.9f);
    }
    public void OpenOptionsMenu()
    {
        mainMenu.DOAnchorPos(new Vector2(0, 500), 0.9f);
        OptionsMenu.DOAnchorPos(new Vector2(0, 0), 0.9f);
    }
    public void CloseOptionsMenu()
    {
        mainMenu.DOAnchorPos(new Vector2(0, 0), 0.9f);
        OptionsMenu.DOAnchorPos(new Vector2(0, -550), 0.9f);
    }
    public void OpenCreditsMenu()
    {
        OptionsMenu.DOAnchorPos(new Vector2(850, 0), 0.9f); 
        CreditsMenu.DOAnchorPos(new Vector2(60, 0), 0.9f);
    }
    public void CloseCreditsMenu()
    {
        CreditsMenu.DOAnchorPos(new Vector2(-800, 0), 0.9f);
        OptionsMenu.DOAnchorPos(new Vector2(-10, 0), 0.9f);
    }
    
}
