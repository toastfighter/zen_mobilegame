﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoCanvasTrigger : MonoBehaviour
{
    public GameObject InfoCanvas;
    // Start is called before the first frame update
    void Start()
    {
        InfoCanvas.SetActive(false);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        InfoCanvas.SetActive(true);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        InfoCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
