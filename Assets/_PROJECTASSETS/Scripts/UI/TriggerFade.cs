﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TriggerFade : MonoBehaviour
{
    public GameObject EndCanvas;
    public Animator FadeAnimator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(FadeCoroutine());
        FadeAnimator.SetBool("isFading", true);
    }

    IEnumerator FadeCoroutine()
    {
        yield return new WaitForSeconds(13);
        Debug.Log("Timewaited");
        EndCanvas.SetActive(true);
        Time.timeScale = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameObject w = GameObject.FindGameObjectWithTag("fade");
        FadeAnimator = w.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
