﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public int mainMenuScene;
    public GameObject pauseMenuCanvas;
    public GameObject gameCanvas;
    public AudioSource backgroundMusic;
    public Slider gameVolumeSlider;
    public float inGameMusicVolume;
    public GameObject StartCanvas;
    public AudioSource inGameMusic;
    public PlayerController playerController;
    public GameObject startBlockCollider;


    private void Update()
    {
        OnGameVolumeChange();
        
    }

    private void Start()
 
   
    {
        GameObject p = GameObject.FindGameObjectWithTag("Player");
        playerController = p.GetComponent<PlayerController>();
        pauseMenuCanvas.SetActive(false);
    }

    public void StartPauseMenu()
    {
        gameCanvas.SetActive(false);
        pauseMenuCanvas.SetActive(true);
        Time.timeScale = 0f;
        backgroundMusic.Pause();
        
    }
    public void ResumeGame()
    {
       pauseMenuCanvas.SetActive(false);
        gameCanvas.SetActive(true);
        Time.timeScale = 1f;
        backgroundMusic.Play();
    }
    public void RestartScene()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
        
    }
    public void BackToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenuScene);
    }
    public void OnGameVolumeChange()
    {
        inGameMusicVolume = gameVolumeSlider.value;
        backgroundMusic.volume = inGameMusicVolume;
    }
    public void BeginGame()
    {


        //playerController.PushForce = 269;
        Time.timeScale = 1;
        inGameMusic.Play();
        StartCanvas.SetActive(false);
        playerController.isGrounded = true;
        startBlockCollider.SetActive(false);
        
    }
  

}   
