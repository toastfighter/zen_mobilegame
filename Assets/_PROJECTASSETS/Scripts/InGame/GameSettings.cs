﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public int textureQuality;
    public float musicVolume;
    public AudioSource musicSource;
    public Dropdown textureQualityDropdown;
    public Slider gameVolumeSlider;
    public GameSettings gameSettings;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
        gameSettings = new GameSettings();

        textureQualityDropdown.onValueChanged.AddListener(delegate { OnTextureQualityChange(); });
        gameVolumeSlider.onValueChanged.AddListener(delegate { OnGameVolumeChange(); });
        OnGameVolumeChange();
        OnTextureQualityChange();
    }
    public void OnTextureQualityChange()
    {
        QualitySettings.masterTextureLimit = gameSettings.textureQuality = textureQualityDropdown.value;
        
    }
    public void OnGameVolumeChange()
    {
        musicVolume = gameVolumeSlider.value;
        musicSource.volume = musicVolume;
        
    }

    // Update is called once per frame
    void Update()
    {
      
       
    }
}
