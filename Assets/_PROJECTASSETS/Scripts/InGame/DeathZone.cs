﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathZone : MonoBehaviour
{
    public GameObject DeathCanvas;
    public GameObject PlayerCam;
    public GameObject DeathCam;

    private void OnTriggerEnter2D(Collider2D collision)
    {
      
        if(collision.gameObject.tag == "Player")
        {
            PlayerCam.SetActive(false);
            DeathCam.SetActive(true);
            DeathCanvas.SetActive(true);
        
          

        }
    }
}
