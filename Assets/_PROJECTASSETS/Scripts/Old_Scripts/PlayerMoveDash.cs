﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveDash : MonoBehaviour
{

    [SerializeField]
    Animator foxAnimator;

    [SerializeField]
    private ParticleSystem dashEffect;

    public Rigidbody2D rb;
    public Vector2 ClimbPos;
    /* public Animator anim; */ //Placeholder
  

    public LayerMask whatIsGround;

    private Collider2D myCollider;
    private bool onGround;
    private int direction;
    private float dashTime;
    public float dashSpeed;
    public float startDashTime;
    public bool canDoubleJump;
    public float PushForce;
    public bool DashInput;
  
  

    public float cooldownTime;
    private float nextDashTime;
    //private float nextJumpTime = 0;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        myCollider = GetComponent<Collider2D>();

        dashEffect.Stop();
    }
    private void Update()
    {
        if(Time.time > nextDashTime)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                DashInput = true;
                nextDashTime = Time.time + cooldownTime;
                foxAnimator.SetBool("isDashing", true);
                foxAnimator.SetBool("isDoubleJumping", false);

                dashEffect.Play();
            }
        }
    }

    private void FixedUpdate()
    {
        onGround = Physics2D.IsTouchingLayers(myCollider, whatIsGround);
        rb.velocity = new Vector2(PushForce, rb.velocity.y);

        //Dash
        if (direction == 0)
        {
            if (DashInput)
            {
                direction = 1;
            }

        }
        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                DashInput = false;                //rb.velocity = Vector2.zero;
                if (startDashTime > 0)
                {
                    rb.velocity = new Vector2(PushForce, rb.velocity.y);
                    foxAnimator.SetBool("isDashing", false);
                    foxAnimator.SetBool("isDoubleJumping", false);
                }
            }
            else
            {

                dashTime -= Time.deltaTime;
                if (direction == 1)
                {
                    rb.velocity = Vector2.right * dashSpeed;
                }
            }

        }
    }

    //Ledge Jump
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if(collision.gameObject.tag == "Ledge")
    //    {
    //        rb.velocity = new Vector2(0, 0);
    //        rb.gravityScale = 0;
    //        ClimbPos = GetComponent<Transform>().position;

    //    }
    //    backtoidle();
    //}
    //private void backtoidle()
    //{
    //    ClimbPos = GetComponent<Transform>().position = new Vector2(ClimbPos.x + 1, ClimbPos.y + 2);
    //    rb.gravityScale = 1;
       
    //}
}
