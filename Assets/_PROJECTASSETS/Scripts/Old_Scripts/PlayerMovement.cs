﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    Animator foxAnimator;


    public float jumpForce;
    public bool grounded;


    public LayerMask whatIsGround;
    private Collider2D myCollider;
    private Rigidbody2D myRigidbody;
    public bool canDoubleJump;

    private bool JumpInput;




    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();


        myCollider = GetComponent<Collider2D>();


    }
    
    private void Update()
    {
        grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);

        if (grounded)
        {
            foxAnimator.SetBool("isRunning", true);
            foxAnimator.SetBool("isJumping", false);
            foxAnimator.SetBool("isDoubleJumping", false);
            
            //foxAnimator.SetBool("isDashing", false);
        }

        if (!grounded)
        {
            foxAnimator.SetBool("isRunning", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            JumpInput = true;
            //foxAnimator.SetBool("isJumping", true);
            //foxAnimator.SetBool("isRunning", false);
            //foxAnimator.SetBool("isDoubleJumping", false);
            //foxAnimator.SetBool("isDashing", false);
        }
    }
    private void FixedUpdate()
    {

        if (JumpInput)
        {
            JumpInput = false;

            if (grounded)
            {
                myRigidbody.velocity = Vector2.up * jumpForce;
                canDoubleJump = true;

                foxAnimator.SetBool("isJumping", true);
                foxAnimator.SetBool("isRunning", false);
                foxAnimator.SetBool("isDoubleJumping", false);
                foxAnimator.SetBool("isDashing", false);

                //JumpInput = false;
            }
            else
            {
                if (canDoubleJump)
                {
                    canDoubleJump = false;
                    myRigidbody.velocity = Vector2.up * jumpForce / 1.58f;
                    Debug.Log("Double Jump");
                    foxAnimator.SetBool("isDoubleJumping", true);
                    foxAnimator.SetBool("isDashing", false);
                    //if (Input.GetKeyDown(KeyCode.Space))
                    //{
                    //    foxAnimator.SetBool("isDoubleJumping", true);
                    //}
                    //(Input.GetKeyDown(KeyCode.Space))


                }

            }

        }



    }
}