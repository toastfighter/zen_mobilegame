﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTouchMovementInput : MonoBehaviour
{
    [SerializeField]
    Animator foxAnimator;

    public float jumpForce;  
   


    public LayerMask whatIsGround;
    private Collider2D myCollider;
    private Rigidbody2D myRigidbody;
    public bool canDoubleJump;
    public bool JumpInput;
    public AudioSource Footsteps;


    public bool grounded;
   

    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();


        myCollider = GetComponent<Collider2D>();


    }

    private void Update()
    {
        grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);

        if (grounded)
        {
            foxAnimator.SetBool("isRunning", true);
            foxAnimator.SetBool("isJumping", false);
            foxAnimator.SetBool("isDoubleJumping", false);
            //foxAnimator.SetBool("isDashing", false);
            
           

        }

        if (!grounded)
        {
            foxAnimator.SetBool("isRunning", false);
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (Input.GetTouch(0).position.x < Screen.width / 2)
            {
                JumpInput = true;
            }
        }
        
    }
    private void FixedUpdate()
    {
        
  
        if (JumpInput)
        {

            //Left Side
            JumpInput = false;

                if (grounded)
                {
                    myRigidbody.velocity = Vector2.up * jumpForce;
                    canDoubleJump = true;

                     foxAnimator.SetBool("isJumping", true);
                     foxAnimator.SetBool("isRunning", false);
                     foxAnimator.SetBool("isDoubleJumping", false);
                     foxAnimator.SetBool("isDashing", false);

                }
                else
                {
                    if (canDoubleJump)
                    {
                        canDoubleJump = false;
                        myRigidbody.velocity = Vector2.up * jumpForce / 1.58f;
                        foxAnimator.SetBool("isDoubleJumping", true);
                        foxAnimator.SetBool("isDashing", false);
                    }
                }
            
        }
    


    }
}