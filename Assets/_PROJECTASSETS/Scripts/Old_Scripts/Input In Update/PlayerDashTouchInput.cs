﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDashTouchInput : MonoBehaviour
{
    [SerializeField]
    Animator foxAnimator;

    public Rigidbody2D rb;
    public Vector2 ClimbPos;
    /* public Animator anim; */ //Placeholder
    [SerializeField]
    private ParticleSystem dashEffect;
    public LayerMask whatIsGround;

    private Collider2D myCollider;
    private bool onGround;
    private int direction;
    private float dashTime;
    public float dashSpeed;
    public float startDashTime;
    public bool canDoubleJump;
    public float PushForce;
    public bool DashInput;
    private bool isGrounded;
    
    float distance = 1.0f;

    
    

    
    
  

    public float cooldownTime;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        myCollider = GetComponent<Collider2D>();

        dashEffect.Stop();

    }
    private void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (Input.GetTouch(0).position.x > Screen.width / 2)
            {
                DashInput = true;
                foxAnimator.SetBool("isDashing", true);
                foxAnimator.SetBool("isDoubleJumping", false);
                dashEffect.Play();
            }
        }
        if (Input.GetKeyDown(KeyCode.A))
          {
            DashInput = true;
            foxAnimator.SetBool("isDashing", true);
            foxAnimator.SetBool("isDoubleJumping", false);
            dashEffect.Play();
        }
    }
    private void FixedUpdate()
    {
        onGround = Physics2D.IsTouchingLayers(myCollider, whatIsGround);
        rb.velocity = new Vector2(PushForce, rb.velocity.y);

        //Dash
        if (direction == 0)
        {
            if (DashInput)
            {
                direction = 1;

            }

        }
        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                //rb.velocity = Vector2.zero;
                DashInput = false;
                if (startDashTime > 0)
                {
                    rb.velocity = new Vector2(PushForce, rb.velocity.y);
                    foxAnimator.SetBool("isDashing", false);
                    foxAnimator.SetBool("isDoubleJumping", false);
                }
            }
            else
            {


                dashTime -= Time.deltaTime;
                if (direction == 1)
                {

                    rb.velocity = Vector2.right * dashSpeed;
                }
            }

        }
    }

    //Ledge Jump
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.tag == "Ledge")
    //    {
    //        rb.velocity = new Vector2(0, 0);
    //        rb.gravityScale = 0;
    //        ClimbPos = GetComponent<Transform>().position;

    //        Debug.Log("Ledge");

    //        //anim.SetTrigger("PlaceHolder");  //Placeholder

    //    }
    //    backtoidle();
    //}
    //private void backtoidle()
    //{
    //    ClimbPos = GetComponent<Transform>().position = new Vector2(ClimbPos.x + 1, ClimbPos.y + 2);
    //    rb.gravityScale = 57;


    //}

}