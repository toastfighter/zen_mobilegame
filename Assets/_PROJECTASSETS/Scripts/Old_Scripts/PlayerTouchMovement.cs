﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTouchMovement : MonoBehaviour
{
    [SerializeField]
    Animator foxAnimator;

    public float jumpForce;  
    public bool grounded;


    public LayerMask whatIsGround;
    private Collider2D myCollider;
    private Rigidbody2D myRigidbody;
    public bool canDoubleJump;

    private bool jumpInput;


    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();


        myCollider = GetComponent<Collider2D>();


    }

    private void Update()
    {
        grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);

        if (grounded)
        {
            foxAnimator.SetBool("isRunning", true);
            foxAnimator.SetBool("isJumping", false);
            foxAnimator.SetBool("isDoubleJumping", false);
        }

        if (!grounded)
        {
            foxAnimator.SetBool("isRunning", false);
        }
    }
    private void FixedUpdate()
    {
        
   
 


        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (Input.GetTouch(0).position.x < Screen.width / 2)
            {
                //Left Side


                if (grounded)
                {
                    myRigidbody.velocity = Vector2.up * jumpForce;
                    canDoubleJump = true;

                    foxAnimator.SetBool("isJumping", true);
                    foxAnimator.SetBool("isRunning", false);
                    foxAnimator.SetBool("isDoubleJumping", false);
                    foxAnimator.SetBool("isDashing", false);
                }
                else
                {
                    if (canDoubleJump)
                    {
                        canDoubleJump = false;
                        myRigidbody.velocity = Vector2.up * jumpForce / 1.58f;

                        foxAnimator.SetBool("isDoubleJumping", true);
                        foxAnimator.SetBool("isDashing", false);
                    }
                }
            }
        }
    


    }
}