﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDashTouch : MonoBehaviour
{
    public Rigidbody2D rb;
    public Vector2 ClimbPos;
    /* public Animator anim; */ //Placeholder


    public LayerMask whatIsGround;

    private Collider2D myCollider;
    private bool onGround;
    private int direction;
    private float dashTime;
    public float dashSpeed;
    public float startDashTime;
    public bool canDoubleJump;
    public float PushForce;




    public float cooldownTime;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        myCollider = GetComponent<Collider2D>();



    }

    private void FixedUpdate()
    {
        onGround = Physics2D.IsTouchingLayers(myCollider, whatIsGround);
        rb.velocity = new Vector2(PushForce, rb.velocity.y);

        //Dash
        if (direction == 0)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(0).position.x > Screen.width / 2)
                {
                    Debug.Log("Dash");
                    direction = 1;
                }
            }

        }
        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                //rb.velocity = Vector2.zero;
                if (startDashTime > 0)
                {
                    rb.velocity = new Vector2(PushForce, rb.velocity.y);
                }
            }
            else
            {


                dashTime -= Time.deltaTime;
                if (direction == 1)
                {

                    rb.velocity = Vector2.right * dashSpeed;
                }
            }

        }
    }

    //Ledge Jump
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.tag == "Ledge")
    //    {
    //        rb.velocity = new Vector2(0, 0);
    //        rb.gravityScale = 0;
    //        ClimbPos = GetComponent<Transform>().position;

    //        Debug.Log("Ledge");

    //        //anim.SetTrigger("PlaceHolder");  //Placeholder

    //    }
    //    backtoidle();
    //}
    //private void backtoidle()
    //{
    //    ClimbPos = GetComponent<Transform>().position = new Vector2(ClimbPos.x + 1, ClimbPos.y + 2);
    //    rb.gravityScale = 57;


    //}

}