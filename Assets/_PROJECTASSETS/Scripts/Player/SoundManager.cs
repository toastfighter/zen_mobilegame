﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource InGameMusic;
    
    // Start is called before the first frame update
    void Start()
    {
   
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        InGameMusic.Play();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        InGameMusic.volume = 0.6f;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
