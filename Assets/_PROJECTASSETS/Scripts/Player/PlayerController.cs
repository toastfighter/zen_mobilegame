using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region SwipeControls

    Vector2 startPos;
    Vector2 endPos;

    #endregion

    #region Public/Private Fields
    public Transform PlayerPosition;
    // public float jumpPower = 1000f;
    //  public float maxJumpTime = 0.50f;
    public float raycastMaxDistance ;
    public float raycastRightDistance;
    // public float walkSpeed = 500f;

    

   

    private const int OBSTACLE_LAYER = 9;

    private Animator foxAnimator;

    public Rigidbody2D body;

    private bool jumpStarted = false;

    public LayerMask whatIsGround;
    public AudioSource inGameMusic;
   

    private float jumpTimeElapsed = 0f;
    public float jumpForce;
    public bool canDoubleJump;

    private float originOffset = 0.5f;
    public bool JumpInput;
    public bool isGrounded;
    public int direction;
    public bool DashInput;
    public ParticleSystem dashEffect;
    public ParticleSystem doubleJumpEffect;
    public float PushForce;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    public float coolDownTime;
    public float nextDashTime = 0;
    public GameObject StartCanvas;

    public bool allowDoubleJump;

    #endregion 

    public void Update()
    {
        #region RayCast
        //// Cast a ray straight down.
        //Vector2 directiondown = new Vector2(0, -1);

        //Ray2D downRay = new Ray2D(transform.position, -Vector2.up);
        //RaycastHit2D hit = Physics2D.Raycast(downRay.origin, downRay.direction, raycastMaxDistance, whatIsGround);

        //Debug.DrawRay(downRay.origin, downRay.direction * raycastMaxDistance, Color.green);


        ////If it hits something...
        //if (hit.collider != null)
        //{

        //    Debug.Log("_GROUNDED");

        //    isGrounded = true;
        //}
        //else
        //{
        //    isGrounded = false;
        //}


        //Vector2 directionright = new Vector2(1, 0);
        //Ray2D rightRay = new Ray2D(transform.position, -Vector2.left);
        //RaycastHit2D hitright = Physics2D.Raycast(rightRay.origin, rightRay.direction, raycastRightDistance, whatIsGround);
        //Debug.DrawRay(rightRay.origin, rightRay.direction * raycastRightDistance, Color.red);

        //if(hitright.collider != null)
        //{
        //    isGrounded = true;
        //}

        #endregion

        #region DashInput
        if (Time.time > nextDashTime)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(0).position.x > Screen.width / 2)
                {
                    nextDashTime = Time.time + coolDownTime;
                    DashInput = true;
                    foxAnimator.SetBool("isDashing", true);
                    foxAnimator.SetBool("isDoubleJumping", false);
                    dashEffect.Play();
                }
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                nextDashTime = Time.time + coolDownTime;
                DashInput = true;
                foxAnimator.SetBool("isDashing", true);
                foxAnimator.SetBool("isDoubleJumping", false);
                dashEffect.Play();
            }
        }
        #endregion

        #region isGrounded

        if (isGrounded)
        {
            Debug.Log("Groundedd");
            
            JumpInput = false;
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(0).position.x < Screen.width / 2 && !JumpInput)
                {


                    JumpInput = true;

                    allowDoubleJump = true;

                }
            }



            if (Input.GetKeyDown(KeyCode.Space) && !JumpInput)
            {
                JumpInput = true;
                allowDoubleJump = true;

            }


            
            foxAnimator.SetBool("isRunning", true);
           
            foxAnimator.SetBool("isJumping", false);
            foxAnimator.SetBool("isDoubleJumping", false);
            //foxAnimator.SetBool("isDashing", false);
                


        }
        else
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(0).position.x < Screen.width / 2 && !JumpInput)
                {

                    canDoubleJump = true;

                }
                else
                {
                    canDoubleJump = false;
                }
            }
            if(Input.GetKeyDown(KeyCode.Space))
            {
                canDoubleJump = true;
            }

        }

        #endregion
        #region isNotGroudned
        if (!isGrounded)
        {
            foxAnimator.SetBool("isRunning", false);
        }

        #endregion
        #region JumpInput

        if (JumpInput)

        {
            JumpInput = false;
            body.velocity = Vector2.up * jumpForce;

            foxAnimator.SetBool("isJumping", true);
            foxAnimator.SetBool("isRunning", false);
            foxAnimator.SetBool("isDoubleJumping", false);
            foxAnimator.SetBool("isDashing", false);

        }

        if (canDoubleJump)
        {
          
            allowDoubleJump = false;
            body.velocity = Vector2.up * jumpForce / 1.2f;
            doubleJumpEffect.Play();
            foxAnimator.SetBool("isDoubleJumping", true);
            foxAnimator.SetBool("isDashing", false);

            canDoubleJump = false;

        }

        #endregion

        

    }

    #region ClimbTrigger & Startholder
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "climbable")
        {
            foxAnimator.SetBool("isClimbing", true);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("SPACEPRESSED");
                body.velocity = new Vector2(0, 20);
            }
        }
        
    }

    #endregion

    #region Startholder
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "startcollider")
        {

            StartCanvas.SetActive(true);
            Time.timeScale = 0;
            //PushForce = 0;
            inGameMusic.Stop();
            isGrounded = false;
            foxAnimator.SetBool("isRunning", false);

        }
    }
    #endregion#



    #region Private Methods

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        foxAnimator = GetComponent<Animator>();
    }

  

    private void FixedUpdate()
    {



        body.velocity = new Vector2(PushForce, body.velocity.y);

        //Dash
        if (direction == 0)
        {
            if (DashInput)
            {
                direction = 1;

            }

        }
        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                //rb.velocity = Vector2.zero;
                DashInput = false;
                if (startDashTime > 0)
                {
                    body.velocity = new Vector2(PushForce, body.velocity.y);
                    foxAnimator.SetBool("isDashing", false);
                    foxAnimator.SetBool("isDoubleJumping", false);
                }
            }
            else
            {


                dashTime -= Time.deltaTime;
                if (direction == 1)
                {

                    body.velocity = Vector2.right * dashSpeed;
                }
            }

        }


    }
}

 
    

    #endregion Private Methods
