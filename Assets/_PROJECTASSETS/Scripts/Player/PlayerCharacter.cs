﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
   
    public Rigidbody2D rb;

    public Transform groundCheck;

    public float groundCheckRadius;

    public LayerMask whatIsGround;

    private bool onGround;

    void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
          rb.velocity = new Vector2(4, rb.velocity.y);
           onGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
             if (Input.GetKey(KeyCode.Space) && onGround)
                {
                    rb.velocity = new Vector2(rb.velocity.x, 2);
                }
        }
    
}
