﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rayshooter : MonoBehaviour
{
    public float raycastMaxDistance;
    public float raycastRightDistance;
    public float raycastIdleDistance;
    public LayerMask whatIsGround;
    public LayerMask IdleGround;
    new PlayerController playerController;
    new Animator foxAnimator;
    public AudioSource PlayerSteps;
    // Start is called before the first frame update
    void Start()
    {
        GameObject p = GameObject.FindGameObjectWithTag("Player");
        playerController = p.GetComponent<PlayerController>();
        foxAnimator = p.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 directiondown = new Vector2(0, -1);

        Ray2D downRay = new Ray2D(transform.position, -Vector2.up);
        RaycastHit2D hit = Physics2D.Raycast(downRay.origin, downRay.direction, raycastMaxDistance, whatIsGround);

        Debug.DrawRay(downRay.origin, downRay.direction * raycastMaxDistance, Color.green);


        //If it hits something...
        if (hit.collider != null)
        {

           

            playerController.isGrounded = true;
            //PlayerSteps.Play();
        }
        else
        {
            playerController.isGrounded = false;
        }

        Vector2 directionright = new Vector2(1, 0);
        Ray2D rightRay = new Ray2D(transform.position, -Vector2.left);
        RaycastHit2D hitright = Physics2D.Raycast(rightRay.origin, rightRay.direction, raycastRightDistance, whatIsGround);
        Debug.DrawRay(rightRay.origin, rightRay.direction * raycastRightDistance, Color.red);

        if (hitright.collider != null)
        {
            playerController.isGrounded = true;
            foxAnimator.SetBool("isRunning", false);
        }

        Vector2 downdirection = new Vector2(1, 0);
        Ray2D rayDown= new Ray2D(transform.position, -Vector2.left);
        RaycastHit2D downHit = Physics2D.Raycast(rightRay.origin, rightRay.direction, raycastIdleDistance, IdleGround);
        Debug.DrawRay(rayDown.origin, rayDown.direction * raycastRightDistance, Color.red);

        if (downHit.collider != null)
        {
            
            foxAnimator.SetBool("isRunning", false);
        }
    }
}
